let SPU = {
	scrollToNextSection: () => {
		let $links = document.querySelectorAll(".js-hero-scroll-down");
		let scrollToNextSection = function () {
			let $wrap = this.closest(".hero-wrap");
			let bodyRect = document.body.getBoundingClientRect();
			let wrapYposition = $wrap.getBoundingClientRect();
			let wrapHeight = $wrap.offsetHeight;
			let scrollAmount = wrapYposition.top - bodyRect.top + wrapHeight;

			window.scroll({
				top: scrollAmount,
				left: 0,
				behavior: 'smooth'
			});
		};

		$links.forEach(el => el.addEventListener("click", scrollToNextSection));
	},

	toggleOffCanvasNav: () => {
		let $links = document.querySelectorAll(".js-mob-nav-toggler");
		let toggleOffCanvasNav = function () {
			this.classList.toggle("is-active");

			let $offcanvas = document.getElementById("offcanvas");
			if ($offcanvas) {
				$offcanvas.classList.toggle("is-active");
			} else {
				alert("Off Canvas Navigation NOT Included!");
			}
		};

		$links.forEach(el => el.addEventListener("click", toggleOffCanvasNav));
	},

	navigateGallery: () => {
		let $galleries = document.querySelectorAll(".carousel-gallery");

		$galleries.forEach(function (el) {
			let $slides = el.querySelectorAll(".carousel-item");
			let slidesLength = $slides.length;
			let $galItems = el.querySelectorAll(".carousel-gallery-item");

			// get total number of slides
			el.querySelector(".carousel-gallery-position-total").innerHTML = slidesLength;

			// on slide change the preview
			el.addEventListener('slid.bs.carousel', function (slide) {
				el.querySelector(".carousel-gallery-position-actual").innerHTML = slide.to + 1; // it starts with 0

				$galItems.forEach(el => el.classList.remove("is-active"));
				el.querySelector(".carousel-gallery-item[data-position='" + slide.to + "']").classList.add("is-active");
			});

			// gallery items change preview on click
			$galItems.forEach(function (item) {
				item.addEventListener('click', function (e) {
					e.preventDefault();

					$galItems.forEach(el => el.classList.remove("is-active"));
					item.classList.add("is-active");

					let position = item.getAttribute("data-position");

					let $preview = el.querySelector("[data-position='" + position + "']");

					$slides.forEach(el => el.classList.remove("active"));
					$preview.classList.add("active");

					el.querySelector(".carousel-gallery-position-actual").innerHTML = parseInt(position) + 1; // it starts with 0
				});
			});
		});
	},

	initCarousel: () => {
		let $glide = document.querySelector(".glide");
		if (typeof ($glide) === 'undefined' || $glide === null) {
			return;
		}

		if (typeof Glide === 'undefined') {
			return;
		}

		let glide = new Glide('.glide', {
			type: 'carousel',
			perView: 5,
			focusAt: 'center',
			breakpoints: {
				800: {
					perView: 3
				},
				480: {
					perView: 1,
					peek: {
						before: 50,
						after: 50,
					}
				}
			}
		});

		glide.mount();
	},

	toggleSearch: () => {
		let $links = document.querySelectorAll(".js-init-search");
		let toggleSearch = function () {
			let $searchWrap = document.querySelector(".search-wrap");
			if ($searchWrap) {
				$searchWrap.classList.toggle("is-active");
			} else {
				alert("Search wrap is NOT included in HTML!");
			}
		};

		$links.forEach(el => el.addEventListener("click", toggleSearch));
	},

	displaySubmenuOnHoverIn: () => {
		let $links = document.querySelectorAll(".topnav .nav-link");
		let hoverSubmenu = function () {
			let target = this.getAttribute("data-submenu");

			SPU.resetSubmenus();

			if( target ) {
				this.classList.add("is-hover");
				document.querySelector(".submenus-wrap").classList.add("is-active");
				document.getElementById(target).style.display = "block";
			}
		};

		$links.forEach(el => el.addEventListener("mouseover", hoverSubmenu));
	},
	
	hideSubmenuOnHoverOut: () => {
		document.querySelector(".submenus-wrap").addEventListener("mouseleave", SPU.resetSubmenus);
		document.querySelector(".logo-wrap").addEventListener("mouseover", SPU.resetSubmenus);
	},

	resetSubmenus: () => {
		document.querySelectorAll(".topnav .nav-link").forEach(el => el.classList.remove("is-hover"));

		let $submenusWrap = document.querySelector(".submenus-wrap");
		if( $submenusWrap.classList.contains("is-active") ){
			$submenusWrap.classList.remove("is-active");
			
			document.querySelectorAll(".submenu-box").forEach( el => el.style.display = "none" );
		}
	},

	closeSubmenu: () => {
		let $links = document.querySelectorAll(".js-close-subnav");
		let closeSubmenuBox = function () {
			SPU.resetSubmenus();
		};

		$links.forEach(el => el.addEventListener("click", closeSubmenuBox));
	},
}

SPU.scrollToNextSection();
SPU.toggleOffCanvasNav();
SPU.navigateGallery();
SPU.initCarousel();
SPU.toggleSearch();
SPU.displaySubmenuOnHoverIn();
SPU.hideSubmenuOnHoverOut();
SPU.closeSubmenu();
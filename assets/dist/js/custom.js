"use strict";

var SPU = {
  scrollToNextSection: function scrollToNextSection() {
    var $links = document.querySelectorAll(".js-hero-scroll-down");

    var scrollToNextSection = function scrollToNextSection() {
      var $wrap = this.closest(".hero-wrap");
      var bodyRect = document.body.getBoundingClientRect();
      var wrapYposition = $wrap.getBoundingClientRect();
      var wrapHeight = $wrap.offsetHeight;
      var scrollAmount = wrapYposition.top - bodyRect.top + wrapHeight;
      window.scroll({
        top: scrollAmount,
        left: 0,
        behavior: 'smooth'
      });
    };

    $links.forEach(function (el) {
      return el.addEventListener("click", scrollToNextSection);
    });
  },
  toggleOffCanvasNav: function toggleOffCanvasNav() {
    var $links = document.querySelectorAll(".js-mob-nav-toggler");

    var toggleOffCanvasNav = function toggleOffCanvasNav() {
      this.classList.toggle("is-active");
      var $offcanvas = document.getElementById("offcanvas");

      if ($offcanvas) {
        $offcanvas.classList.toggle("is-active");
      } else {
        alert("Off Canvas Navigation NOT Included!");
      }
    };

    $links.forEach(function (el) {
      return el.addEventListener("click", toggleOffCanvasNav);
    });
  },
  navigateGallery: function navigateGallery() {
    var $galleries = document.querySelectorAll(".carousel-gallery");
    $galleries.forEach(function (el) {
      var $slides = el.querySelectorAll(".carousel-item");
      var slidesLength = $slides.length;
      var $galItems = el.querySelectorAll(".carousel-gallery-item"); // get total number of slides

      el.querySelector(".carousel-gallery-position-total").innerHTML = slidesLength; // on slide change the preview

      el.addEventListener('slid.bs.carousel', function (slide) {
        el.querySelector(".carousel-gallery-position-actual").innerHTML = slide.to + 1; // it starts with 0

        $galItems.forEach(function (el) {
          return el.classList.remove("is-active");
        });
        el.querySelector(".carousel-gallery-item[data-position='" + slide.to + "']").classList.add("is-active");
      }); // gallery items change preview on click

      $galItems.forEach(function (item) {
        item.addEventListener('click', function (e) {
          e.preventDefault();
          $galItems.forEach(function (el) {
            return el.classList.remove("is-active");
          });
          item.classList.add("is-active");
          var position = item.getAttribute("data-position");
          var $preview = el.querySelector("[data-position='" + position + "']");
          $slides.forEach(function (el) {
            return el.classList.remove("active");
          });
          $preview.classList.add("active");
          el.querySelector(".carousel-gallery-position-actual").innerHTML = parseInt(position) + 1; // it starts with 0
        });
      });
    });
  },
  initCarousel: function initCarousel() {
    var $glide = document.querySelector(".glide");

    if (typeof $glide === 'undefined' || $glide === null) {
      return;
    }

    if (typeof Glide === 'undefined') {
      return;
    }

    var glide = new Glide('.glide', {
      type: 'carousel',
      perView: 5,
      focusAt: 'center',
      breakpoints: {
        800: {
          perView: 3
        },
        480: {
          perView: 1,
          peek: {
            before: 50,
            after: 50
          }
        }
      }
    });
    glide.mount();
  },
  toggleSearch: function toggleSearch() {
    var $links = document.querySelectorAll(".js-init-search");

    var toggleSearch = function toggleSearch() {
      var $searchWrap = document.querySelector(".search-wrap");

      if ($searchWrap) {
        $searchWrap.classList.toggle("is-active");
      } else {
        alert("Search wrap is NOT included in HTML!");
      }
    };

    $links.forEach(function (el) {
      return el.addEventListener("click", toggleSearch);
    });
  },
  displaySubmenuOnHoverIn: function displaySubmenuOnHoverIn() {
    var $links = document.querySelectorAll(".topnav .nav-link");

    var hoverSubmenu = function hoverSubmenu() {
      var target = this.getAttribute("data-submenu");
      SPU.resetSubmenus();

      if (target) {
        this.classList.add("is-hover");
        document.querySelector(".submenus-wrap").classList.add("is-active");
        document.getElementById(target).style.display = "block";
      }
    };

    $links.forEach(function (el) {
      return el.addEventListener("mouseover", hoverSubmenu);
    });
  },
  hideSubmenuOnHoverOut: function hideSubmenuOnHoverOut() {
    document.querySelector(".submenus-wrap").addEventListener("mouseleave", SPU.resetSubmenus);
    document.querySelector(".logo-wrap").addEventListener("mouseover", SPU.resetSubmenus);
  },
  resetSubmenus: function resetSubmenus() {
    document.querySelectorAll(".topnav .nav-link").forEach(function (el) {
      return el.classList.remove("is-hover");
    });
    var $submenusWrap = document.querySelector(".submenus-wrap");

    if ($submenusWrap.classList.contains("is-active")) {
      $submenusWrap.classList.remove("is-active");
      document.querySelectorAll(".submenu-box").forEach(function (el) {
        return el.style.display = "none";
      });
    }
  },
  closeSubmenu: function closeSubmenu() {
    var $links = document.querySelectorAll(".js-close-subnav");

    var closeSubmenuBox = function closeSubmenuBox() {
      SPU.resetSubmenus();
    };

    $links.forEach(function (el) {
      return el.addEventListener("click", closeSubmenuBox);
    });
  }
};
SPU.scrollToNextSection();
SPU.toggleOffCanvasNav();
SPU.navigateGallery();
SPU.initCarousel();
SPU.toggleSearch();
SPU.displaySubmenuOnHoverIn();
SPU.hideSubmenuOnHoverOut();
SPU.closeSubmenu();
